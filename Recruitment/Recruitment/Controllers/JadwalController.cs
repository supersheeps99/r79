﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Recruitment.DAL;

namespace Recruitment.Controllers
{
    public class JadwalController : Controller
    {
        private RecruitmentDbContext db = new RecruitmentDbContext();

        // GET: Jadwal
        public ActionResult Index()
        {
            return View(db.tbJadwal.ToList());
        }

        // GET: Jadwal/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbJadwal tbJadwal = db.tbJadwal.Find(id);
            if (tbJadwal == null)
            {
                return HttpNotFound();
            }
            return View(tbJadwal);
        }

        // GET: Jadwal/Create
        public ActionResult Create()
        {
            //dropdown list
            ViewBag.Kandidat = new SelectList(db.tbKandidat, "KodeKandidat", "Nama");
            return View();
        }

        // POST: Jadwal/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KodeJadwalPemanggilan,KodeKandidat,JenisPemanggilan,TanggalPemanggilan,TanggalKedatangan,KonfirmasiKedatangan,TanggalDibuat,Pengguna")] tbJadwal tbJadwal)
        {
            if (ModelState.IsValid)
            {
                tbJadwal.TanggalDibuat = DateTime.Now.Date;
                tbJadwal.Pengguna = "Admin";
                db.tbJadwal.Add(tbJadwal);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbJadwal);
        }

        // GET: Jadwal/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbJadwal tbJadwal = db.tbJadwal.Find(id);
            if (tbJadwal == null)
            {
                return HttpNotFound();
            }
            return View(tbJadwal);
        }

        // POST: Jadwal/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "KodeJadwalPemanggilan,KodeKandidat,JenisPemanggilan,TanggalPemanggilan,TanggalKedatangan,KonfirmasiKedatangan,TanggalDibuat,Pengguna")] tbJadwal tbJadwal)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbJadwal).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbJadwal);
        }

        // GET: Jadwal/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbJadwal tbJadwal = db.tbJadwal.Find(id);
            if (tbJadwal == null)
            {
                return HttpNotFound();
            }
            return View(tbJadwal);
        }

        // POST: Jadwal/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbJadwal tbJadwal = db.tbJadwal.Find(id);
            db.tbJadwal.Remove(tbJadwal);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
