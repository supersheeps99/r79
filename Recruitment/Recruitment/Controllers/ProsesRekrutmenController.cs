﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Recruitment.DAL;

namespace Recruitment.Controllers
{
    public class ProsesRekrutmenController : Controller
    {
        private RecruitmentDbContext db = new RecruitmentDbContext();

        // GET: ProsesRekrutmen
        public ActionResult Index()
        {
            var tbProsesRekrutmen = db.tbProsesRekrutmen.Include(t => t.tbKandidat);
            return View(tbProsesRekrutmen.ToList());
        }

        // GET: ProsesRekrutmen/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbProsesRekrutmen tbProsesRekrutmen = db.tbProsesRekrutmen.Find(id);
            if (tbProsesRekrutmen == null)
            {
                return HttpNotFound();
            }
            return View(tbProsesRekrutmen);
        }

        // GET: ProsesRekrutmen/Create
        public ActionResult Create()
        {
            ViewBag.KodeKandidat = new SelectList(db.tbKandidat, "KodeKandidat", "Nama");
            return View();
        }

        // POST: ProsesRekrutmen/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KodeProsesRekrutmen,TanggalProsesRekrutmen,KodeKandidat,KodeNilaiAptitude,ProsesStep")] tbProsesRekrutmen tbProsesRekrutmen)
        {
            if (ModelState.IsValid)
            {
                tbProsesRekrutmen.TanggalDibuat = DateTime.Now;
                tbProsesRekrutmen.Pengguna = "Admin";

                db.tbProsesRekrutmen.Add(tbProsesRekrutmen);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.KodeKandidat = new SelectList(db.tbKandidat, "KodeKandidat", "Nama", tbProsesRekrutmen.KodeKandidat);
            return View(tbProsesRekrutmen);
        }

        // GET: ProsesRekrutmen/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbProsesRekrutmen tbProsesRekrutmen = db.tbProsesRekrutmen.Find(id);
            if (tbProsesRekrutmen == null)
            {
                return HttpNotFound();
            }
            ViewBag.KodeKandidat = new SelectList(db.tbKandidat, "KodeKandidat", "Nama", tbProsesRekrutmen.KodeKandidat);
            return View(tbProsesRekrutmen);
        }

        // POST: ProsesRekrutmen/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "KodeProsesRekrutmen,TanggalProsesRekrutmen,KodeKandidat,KodeNilaiAptitude,ProsesStep,TanggalDibuat,Pengguna")] tbProsesRekrutmen tbProsesRekrutmen)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbProsesRekrutmen).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KodeKandidat = new SelectList(db.tbKandidat, "KodeKandidat", "Nama", tbProsesRekrutmen.KodeKandidat);
            return View(tbProsesRekrutmen);
        }

        // GET: ProsesRekrutmen/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbProsesRekrutmen tbProsesRekrutmen = db.tbProsesRekrutmen.Find(id);
            if (tbProsesRekrutmen == null)
            {
                return HttpNotFound();
            }
            return View(tbProsesRekrutmen);
        }

        // POST: ProsesRekrutmen/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbProsesRekrutmen tbProsesRekrutmen = db.tbProsesRekrutmen.Find(id);
            db.tbProsesRekrutmen.Remove(tbProsesRekrutmen);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
