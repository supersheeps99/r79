﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Recruitment.DAL;

namespace Recruitment.Controllers
{
    public class KandidatController : Controller
    {
        private RecruitmentDbContext db = new RecruitmentDbContext();

        // GET: Kandidat
        public ActionResult Index()
        {
            return View(db.tbKandidat.ToList());
        }

        // GET: Kandidat/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbKandidat tbKandidat = db.tbKandidat.Find(id);
            if (tbKandidat == null)
            {
                return HttpNotFound();
            }
            return View(tbKandidat);
        }

        // GET: Kandidat/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Kandidat/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KodeKandidat,Nama,TempatLahir,TanggalLahir,JenisKelamin,Agama,Kewarganegaraan,NoTelpon,KotaTinggal,JabatanYangDiajukan,PengalamanTeknologi,PendidikanTerakhir,LamaPengalamanTahunBekerja,TanggalInput,KodeDokumen,TanggalDibuat,Pengguna")] tbKandidat tbKandidat)
        {
            if (ModelState.IsValid)
            {
                //untuk isi tanggal di buat dan pengguna
                tbKandidat.TanggalInput = DateTime.Now.Date;
                tbKandidat.TanggalDibuat = DateTime.Now.Date;
                tbKandidat.Pengguna = "Admin";
                db.tbKandidat.Add(tbKandidat);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbKandidat);
        }

        // GET: Kandidat/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbKandidat tbKandidat = db.tbKandidat.Find(id);
            if (tbKandidat == null)
            {
                return HttpNotFound();
            }
            return View(tbKandidat);
        }

        // POST: Kandidat/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "KodeKandidat,Nama,TempatLahir,TanggalLahir,JenisKelamin,Agama,Kewarganegaraan,NoTelpon,KotaTinggal,JabatanYangDiajukan,PengalamanTeknologi,PendidikanTerakhir,LamaPengalamanTahunBekerja,TanggalInput,KodeDokumen,TanggalDibuat,Pengguna")] tbKandidat tbKandidat)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbKandidat).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbKandidat);
        }

        // GET: Kandidat/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbKandidat tbKandidat = db.tbKandidat.Find(id);
            if (tbKandidat == null)
            {
                return HttpNotFound();
            }
            return View(tbKandidat);
        }

        // POST: Kandidat/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbKandidat tbKandidat = db.tbKandidat.Find(id);
            db.tbKandidat.Remove(tbKandidat);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
