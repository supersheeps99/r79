﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Recruitment.DAL;

namespace Recruitment.Controllers
{
    public class WawancaraController : Controller
    {
        private RecruitmentDbContext db = new RecruitmentDbContext();

        // GET: Wawancara
        public ActionResult Index()
        {
            var tbWawancara = db.tbWawancara.Include(t => t.tbProsesRekrutmen);
            return View(tbWawancara.ToList());
        }

        // GET: Wawancara/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbWawancara tbWawancara = db.tbWawancara.Find(id);
            if (tbWawancara == null)
            {
                return HttpNotFound();
            }
            return View(tbWawancara);
        }

        // GET: Wawancara/Create
        public ActionResult Create()
        {
            ViewBag.KodeProsesRekrutmen = new SelectList(db.vProsesRekrutmenKandidat, "KodeProsesRekrutmen", "Nama");
            return View();
        }

        // POST: Wawancara/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KodeWawancara,KodeProsesRekrutmen,TanggalWawancara,DeskripsiHasilWawancara,KesimpulanWawancara,TanggalDibuat,Pengguna")] tbWawancara tbWawancara)
        {
            if (ModelState.IsValid)
            {
                tbWawancara.TanggalDibuat = DateTime.Now.Date;
                tbWawancara.Pengguna = "Admin";
                db.tbWawancara.Add(tbWawancara);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.KodeProsesRekrutmen = new SelectList(db.tbProsesRekrutmen, "KodeProsesRekrutmen", "Pengguna", tbWawancara.KodeProsesRekrutmen);
            return View(tbWawancara);
        }

        // GET: Wawancara/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbWawancara tbWawancara = db.tbWawancara.Find(id);
            if (tbWawancara == null)
            {
                return HttpNotFound();
            }
            ViewBag.KodeProsesRekrutmen = new SelectList(db.vProsesRekrutmenKandidat, "KodeProsesRekrutmen", "Nama", tbWawancara.KodeProsesRekrutmen);
            return View(tbWawancara);
        }

        // POST: Wawancara/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "KodeWawancara,KodeProsesRekrutmen,TanggalWawancara,DeskripsiHasilWawancara,KesimpulanWawancara")] tbWawancara tbWawancara)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbWawancara).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KodeProsesRekrutmen = new SelectList(db.tbProsesRekrutmen, "KodeProsesRekrutmen", "Pengguna", tbWawancara.KodeProsesRekrutmen);
            return View(tbWawancara);
        }

        // GET: Wawancara/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbWawancara tbWawancara = db.tbWawancara.Find(id);
            if (tbWawancara == null)
            {
                return HttpNotFound();
            }
            return View(tbWawancara);
        }

        // POST: Wawancara/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbWawancara tbWawancara = db.tbWawancara.Find(id);
            db.tbWawancara.Remove(tbWawancara);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
