﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Recruitment.DAL;

namespace Recruitment.Controllers
{
    public class PenilaianHasilTestController : Controller
    {
        private RecruitmentDbContext db = new RecruitmentDbContext();

        // GET: PenilaianHasilTest
        public ActionResult Index()
        {
            var tbPenilaianHasilTest = db.tbPenilaianHasilTest.Include(t => t.tbProsesRekrutmen);
            return View(tbPenilaianHasilTest.ToList());
        }

        // GET: PenilaianHasilTest/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbPenilaianHasilTest tbPenilaianHasilTest = db.tbPenilaianHasilTest.Find(id);
            if (tbPenilaianHasilTest == null)
            {
                return HttpNotFound();
            }
            return View(tbPenilaianHasilTest);
        }

        // GET: PenilaianHasilTest/Create
        public ActionResult Create()
        {
            try
            {
                if (!string.IsNullOrEmpty(Url.RequestContext.RouteData.Values["id"].ToString()))
                {
                    int id = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"].ToString());
                    ViewBag.KodeProsesRekrutmen = new SelectList(db.vProsesRekrutmenKandidat.Where(dt => dt.KodeProsesRekrutmen == id), "KodeProsesRekrutmen", "Nama");
                    var vTanggalProsesRekrutmen = db.vProsesRekrutmenKandidat.Where(dt => dt.KodeProsesRekrutmen == id).Select(dt => dt.TanggalProsesRekrutmen).SingleOrDefault();
                    ViewBag.TanggalProsesRekrutmen = vTanggalProsesRekrutmen.ToString();
                    ViewBag.Display = "";
                }
                else
                {
                    ViewBag.KodeProsesRekrutmen = new SelectList(db.vProsesRekrutmenKandidat, "KodeProsesRekrutmen", "Nama");
                    ViewBag.Display = "style=display:none";
                }
            }
            catch (Exception)
            {
                ViewBag.KodeProsesRekrutmen = new SelectList(db.vProsesRekrutmenKandidat, "KodeProsesRekrutmen", "Nama");
                ViewBag.TanggalProsesRekrutmen = DateTime.Now.Date.ToShortDateString();
                ViewBag.Display = "style=display:none";
            }
            return View();
        }

        // POST: PenilaianHasilTest/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KodePenilaianHasilTes,KodeProsesRekrutmen,TanggalPenilaian,StatusTest,Keterangan,TanggalDibuat,Pengguna")] tbPenilaianHasilTest tbPenilaianHasilTest)
        {
            if (ModelState.IsValid)
            {
                db.tbPenilaianHasilTest.Add(tbPenilaianHasilTest);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.KodeProsesRekrutmen = new SelectList(db.tbProsesRekrutmen, "KodeProsesRekrutmen", "Pengguna", tbPenilaianHasilTest.KodeProsesRekrutmen);
            return View(tbPenilaianHasilTest);
        }

        // GET: PenilaianHasilTest/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbPenilaianHasilTest tbPenilaianHasilTest = db.tbPenilaianHasilTest.Find(id);
            if (tbPenilaianHasilTest == null)
            {
                return HttpNotFound();
            }
            ViewBag.KodeProsesRekrutmen = new SelectList(db.tbProsesRekrutmen, "KodeProsesRekrutmen", "Pengguna", tbPenilaianHasilTest.KodeProsesRekrutmen);
            return View(tbPenilaianHasilTest);
        }

        // POST: PenilaianHasilTest/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "KodePenilaianHasilTes,KodeProsesRekrutmen,TanggalPenilaian,StatusTest,Keterangan,TanggalDibuat,Pengguna")] tbPenilaianHasilTest tbPenilaianHasilTest)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbPenilaianHasilTest).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KodeProsesRekrutmen = new SelectList(db.tbProsesRekrutmen, "KodeProsesRekrutmen", "Pengguna", tbPenilaianHasilTest.KodeProsesRekrutmen);
            return View(tbPenilaianHasilTest);
        }

        // GET: PenilaianHasilTest/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbPenilaianHasilTest tbPenilaianHasilTest = db.tbPenilaianHasilTest.Find(id);
            if (tbPenilaianHasilTest == null)
            {
                return HttpNotFound();
            }
            return View(tbPenilaianHasilTest);
        }

        // POST: PenilaianHasilTest/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbPenilaianHasilTest tbPenilaianHasilTest = db.tbPenilaianHasilTest.Find(id);
            db.tbPenilaianHasilTest.Remove(tbPenilaianHasilTest);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
