﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Recruitment.DAL;

namespace Recruitment.Controllers
{
    public class NilaiPseudocodeController : Controller
    {
        private RecruitmentDbContext db = new RecruitmentDbContext();

        // GET: NilaiPseudocode
        public ActionResult Index()
        {
            var tbNilaiPseudocode = db.tbNilaiPseudocode.Include(t => t.tbProsesRekrutmen);
            return View(tbNilaiPseudocode.ToList());
        }

        // GET: NilaiPseudocode/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbNilaiPseudocode tbNilaiPseudocode = db.tbNilaiPseudocode.Find(id);
            if (tbNilaiPseudocode == null)
            {
                return HttpNotFound();
            }
            return View(tbNilaiPseudocode);
        }

        // GET: NilaiPseudocode/Create
        public ActionResult Create()
        {
            try
            {
                if (!string.IsNullOrEmpty(Url.RequestContext.RouteData.Values["id"].ToString()))
                {
                    int id = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"].ToString());
                    ViewBag.KodeProsesRekrutmen = new SelectList(db.vProsesRekrutmenKandidat.Where(dt => dt.KodeProsesRekrutmen == id), "KodeProsesRekrutmen", "Nama");
                    var vTanggalProsesRekrutmen = db.vProsesRekrutmenKandidat.Where(dt => dt.KodeProsesRekrutmen == id).Select(dt => dt.TanggalProsesRekrutmen).SingleOrDefault();
                    ViewBag.TanggalProsesRekrutmen = vTanggalProsesRekrutmen.ToString();
                    ViewBag.Display = "";
                }
                else
                {
                    ViewBag.KodeProsesRekrutmen = new SelectList(db.vProsesRekrutmenKandidat, "KodeProsesRekrutmen", "Nama");
                    ViewBag.Display = "style=display:none";
                }
            }
            catch (Exception)
            {
                ViewBag.KodeProsesRekrutmen = new SelectList(db.vProsesRekrutmenKandidat, "KodeProsesRekrutmen", "Nama");
                ViewBag.TanggalProsesRekrutmen = DateTime.Now.Date.ToShortDateString();
                ViewBag.Display = "style=display:none";
            }
            return View();
        }

        // POST: NilaiPseudocode/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KodeNilaiPsudecode,KodeProsesRekrutmen,NoSoal,DeskripsiHasilTest,Kesimpulan,GradeNilai,TanggalDibuat,Pengguna")] tbNilaiPseudocode tbNilaiPseudocode)
        {
            if (ModelState.IsValid)
            {
                tbNilaiPseudocode.TanggalDibuat = DateTime.Now.Date;
                tbNilaiPseudocode.Pengguna = "Admin";
                db.tbNilaiPseudocode.Add(tbNilaiPseudocode);
                db.SaveChanges();
                return RedirectToAction("Index", "ProsesRekrutmen");
            }

            ViewBag.KodeProsesRekrutmen = new SelectList(db.tbProsesRekrutmen, "KodeProsesRekrutmen", "Pengguna", tbNilaiPseudocode.KodeProsesRekrutmen);
            return View(tbNilaiPseudocode);
        }

        // GET: NilaiPseudocode/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbNilaiPseudocode tbNilaiPseudocode = db.tbNilaiPseudocode.Find(id);
            if (tbNilaiPseudocode == null)
            {
                return HttpNotFound();
            }
            ViewBag.KodeProsesRekrutmen = new SelectList(db.tbProsesRekrutmen, "KodeProsesRekrutmen", "Pengguna", tbNilaiPseudocode.KodeProsesRekrutmen);
            return View(tbNilaiPseudocode);
        }

        // POST: NilaiPseudocode/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "KodeNilaiPsudecode,KodeProsesRekrutmen,NoSoal,DeskripsiHasilTest,Kesimpulan,GradeNilai,TanggalDibuat,Pengguna")] tbNilaiPseudocode tbNilaiPseudocode)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbNilaiPseudocode).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KodeProsesRekrutmen = new SelectList(db.tbProsesRekrutmen, "KodeProsesRekrutmen", "Pengguna", tbNilaiPseudocode.KodeProsesRekrutmen);
            return View(tbNilaiPseudocode);
        }

        // GET: NilaiPseudocode/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbNilaiPseudocode tbNilaiPseudocode = db.tbNilaiPseudocode.Find(id);
            if (tbNilaiPseudocode == null)
            {
                return HttpNotFound();
            }
            return View(tbNilaiPseudocode);
        }

        // POST: NilaiPseudocode/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbNilaiPseudocode tbNilaiPseudocode = db.tbNilaiPseudocode.Find(id);
            db.tbNilaiPseudocode.Remove(tbNilaiPseudocode);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
