﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Recruitment.DAL;

namespace Recruitment.Controllers
{
    public class PeranController : Controller
    {
        private RecruitmentDbContext db = new RecruitmentDbContext();

        // GET: Peran
        public ActionResult Index()
        {
            return View(db.tbAdmPeran.ToList());
        }

        // GET: Peran/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbAdmPeran tbAdmPeran = db.tbAdmPeran.Find(id);
            if (tbAdmPeran == null)
            {
                return HttpNotFound();
            }
            return View(tbAdmPeran);
        }

        // GET: Peran/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Peran/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KodePeran,NamaPeran")] tbAdmPeran tbAdmPeran)
        {
            if (ModelState.IsValid)
            {
                db.tbAdmPeran.Add(tbAdmPeran);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbAdmPeran);
        }

        // GET: Peran/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbAdmPeran tbAdmPeran = db.tbAdmPeran.Find(id);
            if (tbAdmPeran == null)
            {
                return HttpNotFound();
            }
            return View(tbAdmPeran);
        }

        // POST: Peran/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "KodePeran,NamaPeran")] tbAdmPeran tbAdmPeran)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbAdmPeran).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbAdmPeran);
        }

        // GET: Peran/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbAdmPeran tbAdmPeran = db.tbAdmPeran.Find(id);
            if (tbAdmPeran == null)
            {
                return HttpNotFound();
            }
            return View(tbAdmPeran);
        }

        // POST: Peran/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbAdmPeran tbAdmPeran = db.tbAdmPeran.Find(id);
            db.tbAdmPeran.Remove(tbAdmPeran);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
