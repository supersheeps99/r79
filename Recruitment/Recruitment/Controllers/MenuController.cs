﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Recruitment.DAL;

namespace Recruitment.Controllers
{
    public class MenuController : Controller
    {
        private RecruitmentDbContext db = new RecruitmentDbContext();

        // GET: Menu
        public ActionResult Index()
        {
            return View(db.tbAdmMenu.ToList());
        }

        // GET: Menu/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbAdmMenu tbAdmMenu = db.tbAdmMenu.Find(id);
            if (tbAdmMenu == null)
            {
                return HttpNotFound();
            }
            return View(tbAdmMenu);
        }

        // GET: Menu/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Menu/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KodeMenu,NamaMenu,JenisMenu,KodeMenuParent,Controller,Action,Link,UrutanMenu,IsAktif,TanggalDibuat,Pengguna")] tbAdmMenu tbAdmMenu)
        {
            if (ModelState.IsValid)
            {
                tbAdmMenu.TanggalDibuat = DateTime.Now.Date;
                tbAdmMenu.Pengguna = "Admin";
                db.tbAdmMenu.Add(tbAdmMenu);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbAdmMenu);
        }

        // GET: Menu/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbAdmMenu tbAdmMenu = db.tbAdmMenu.Find(id);
            if (tbAdmMenu == null)
            {
                return HttpNotFound();
            }
            return View(tbAdmMenu);
        }

        // POST: Menu/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "KodeMenu,NamaMenu,JenisMenu,KodeMenuParent,Controller,Action,Link,UrutanMenu,IsAktif,TanggalDibuat,Pengguna")] tbAdmMenu tbAdmMenu)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbAdmMenu).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbAdmMenu);
        }

        // GET: Menu/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbAdmMenu tbAdmMenu = db.tbAdmMenu.Find(id);
            if (tbAdmMenu == null)
            {
                return HttpNotFound();
            }
            return View(tbAdmMenu);
        }

        // POST: Menu/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbAdmMenu tbAdmMenu = db.tbAdmMenu.Find(id);
            db.tbAdmMenu.Remove(tbAdmMenu);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
