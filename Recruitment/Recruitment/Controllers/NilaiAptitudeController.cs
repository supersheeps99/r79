﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Recruitment.DAL;

namespace Recruitment.Controllers
{
    public class NilaiAptitudeController : Controller
    {
        private RecruitmentDbContext db = new RecruitmentDbContext();

        // GET: NilaiAptitude
        public ActionResult Index()
        {
            return View(db.tbNilaiAptitude.ToList());
        }

        // GET: NilaiAptitude/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbNilaiAptitude tbNilaiAptitude = db.tbNilaiAptitude.Find(id);
            if (tbNilaiAptitude == null)
            {
                return HttpNotFound();
            }
            return View(tbNilaiAptitude);
        }

        // GET: NilaiAptitude/Create
        public ActionResult Create()
        {
            try
            {
                if (!string.IsNullOrEmpty(Url.RequestContext.RouteData.Values["id"].ToString()))
                {
                    int id = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"].ToString());
                    ViewBag.KodeProsesRekrutmen = new SelectList(db.vProsesRekrutmenKandidat.Where(dt => dt.KodeProsesRekrutmen == id), "KodeProsesRekrutmen", "Nama");
                    var vTanggalProsesRekrutmen = db.vProsesRekrutmenKandidat.Where(dt => dt.KodeProsesRekrutmen == id).Select(dt => dt.TanggalProsesRekrutmen).SingleOrDefault();
                    ViewBag.TanggalProsesRekrutmen = vTanggalProsesRekrutmen.ToString();
                    ViewBag.Display = "";
                }
                else
                {
                    ViewBag.KodeProsesRekrutmen = new SelectList(db.vProsesRekrutmenKandidat, "KodeProsesRekrutmen", "Nama");
                    ViewBag.Display = "style=display:none";
                }
            }
            catch (Exception)
            {
                ViewBag.KodeProsesRekrutmen = new SelectList(db.vProsesRekrutmenKandidat, "KodeProsesRekrutmen", "Nama");
                ViewBag.TanggalProsesRekrutmen = DateTime.Now.Date.ToShortDateString();
                ViewBag.Display = "style=display:none";
            }

            return View();
        }

        // POST: NilaiAptitude/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KodeNilaiAptitude,KodeProsesRekrutmen,NilaiAll,NilaiKompleks,NilaiLogika,NilaiPola,Grade,TanggalDibuat,Pengguna")] tbNilaiAptitude tbNilaiAptitude)
        {
            if (ModelState.IsValid)
            {
                tbNilaiAptitude.TanggalDibuat = DateTime.Now.Date;
                tbNilaiAptitude.Pengguna = "Admin";
                db.tbNilaiAptitude.Add(tbNilaiAptitude);
                db.SaveChanges();
                return RedirectToAction("Index", "ProsesRekrutmen");
            }

            return View(tbNilaiAptitude);
        }

        // GET: NilaiAptitude/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbNilaiAptitude tbNilaiAptitude = db.tbNilaiAptitude.Find(id);
            if (tbNilaiAptitude == null)
            {
                return HttpNotFound();
            }
            return View(tbNilaiAptitude);
        }

        // POST: NilaiAptitude/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "KodeNilaiAptitude,KodeProsesRekrutmen,NilaiAll,NilaiKompleks,NilaiLogika,NilaiPola,Grade,TanggalDibuat,Pengguna")] tbNilaiAptitude tbNilaiAptitude)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbNilaiAptitude).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbNilaiAptitude);
        }

        // GET: NilaiAptitude/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbNilaiAptitude tbNilaiAptitude = db.tbNilaiAptitude.Find(id);
            if (tbNilaiAptitude == null)
            {
                return HttpNotFound();
            }
            return View(tbNilaiAptitude);
        }

        // POST: NilaiAptitude/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbNilaiAptitude tbNilaiAptitude = db.tbNilaiAptitude.Find(id);
            db.tbNilaiAptitude.Remove(tbNilaiAptitude);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
