﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Recruitment.DAL;

namespace Recruitment.Controllers
{
    public class AksesMenuController : Controller
    {
        private RecruitmentDbContext db = new RecruitmentDbContext();

        // GET: AksesMenu
        public ActionResult Index()
        {
            var tbAdmAksesMenu = db.tbAdmAksesMenu.Include(t => t.tbAdmLogin).Include(t => t.tbAdmMenu);
            return View(tbAdmAksesMenu.ToList());
        }

        // GET: AksesMenu/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbAdmAksesMenu tbAdmAksesMenu = db.tbAdmAksesMenu.Find(id);
            if (tbAdmAksesMenu == null)
            {
                return HttpNotFound();
            }
            return View(tbAdmAksesMenu);
        }

        // GET: AksesMenu/Create
        public ActionResult Create()
        {
            ViewBag.UserName = new SelectList(db.tbAdmLogin, "UserName", "Password");
            ViewBag.KodeMenu = new SelectList(db.tbAdmMenu, "KodeMenu", "NamaMenu");
            return View();
        }

        // POST: AksesMenu/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KodeAkses,UserName,KodeMenu,TanggalDibuat,Pengguna")] tbAdmAksesMenu tbAdmAksesMenu)
        {
            if (ModelState.IsValid)
            {
                tbAdmAksesMenu.TanggalDibuat = DateTime.Now.Date;
                tbAdmAksesMenu.Pengguna = "Admin";
                db.tbAdmAksesMenu.Add(tbAdmAksesMenu);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserName = new SelectList(db.tbAdmLogin, "UserName", "UserName", tbAdmAksesMenu.UserName);
            ViewBag.KodeMenu = new SelectList(db.tbAdmMenu, "KodeMenu", "NamaMenu", tbAdmAksesMenu.KodeMenu);
            return View(tbAdmAksesMenu);
        }

        // GET: AksesMenu/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbAdmAksesMenu tbAdmAksesMenu = db.tbAdmAksesMenu.Find(id);
            if (tbAdmAksesMenu == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserName = new SelectList(db.tbAdmLogin, "UserName", "UserName", tbAdmAksesMenu.UserName);
            ViewBag.KodeMenu = new SelectList(db.tbAdmMenu, "KodeMenu", "NamaMenu", tbAdmAksesMenu.KodeMenu);
            return View(tbAdmAksesMenu);
        }

        // POST: AksesMenu/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "KodeAkses,UserName,KodeMenu,TanggalDibuat,Pengguna")] tbAdmAksesMenu tbAdmAksesMenu)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbAdmAksesMenu).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserName = new SelectList(db.tbAdmLogin, "UserName", "UserName", tbAdmAksesMenu.UserName);
            ViewBag.KodeMenu = new SelectList(db.tbAdmMenu, "KodeMenu", "NamaMenu", tbAdmAksesMenu.KodeMenu);
            return View(tbAdmAksesMenu);
        }

        // GET: AksesMenu/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbAdmAksesMenu tbAdmAksesMenu = db.tbAdmAksesMenu.Find(id);
            if (tbAdmAksesMenu == null)
            {
                return HttpNotFound();
            }
            return View(tbAdmAksesMenu);
        }

        // POST: AksesMenu/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbAdmAksesMenu tbAdmAksesMenu = db.tbAdmAksesMenu.Find(id);
            db.tbAdmAksesMenu.Remove(tbAdmAksesMenu);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
