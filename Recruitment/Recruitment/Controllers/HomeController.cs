﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Recruitment.Models;
using System.Dynamic;
using Recruitment.DAL;
using System.IO;
using System.Web.Mvc.Html;

namespace Recruitment.Controllers
{
    public class HomeController : Controller
    {
        private RecruitmentDbContext db = new RecruitmentDbContext();

        public ActionResult Index()
        {
            ViewBag.GetUserLogon = "Administrator";
            //dynamic myModel = new ExpandoObject();
            //myModel.MenuOtorisasi = GetMenuOtorisasi();
            //ViewModel myModel = new ViewModel();
            //myModel.MenuOtorisasi = GetMenuOtorisasi();
            Session["GetJadwalRekrutmenMingguIni"] = GetJadwalRekrutmenMingguIni();
            Session["GetJadwalRekrutmenBulanIni"] = GetJadwalRekrutmenBulanIni();
            Session["GetJadwalRekrutmenBulanDepan"] = GetJadwalRekrutmenBulanDepan();
            //MenuHtml();
            Session["MenuOtorisasi"] = GetMenuOtorisasi();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public string GetTestMethod(string NamaUser)
        {
            string retValue = string.Empty;

            retValue = "Asep";

            return retValue;
        }

        public List<MenuOtorisasi> GetMenuOtorisasi()
        {
            using (var db = new RecruitmentDbContext())
            {
                List<MenuOtorisasi> List = new List<MenuOtorisasi>();
                var ListMenuOtorisasi = from Akses in db.tbAdmAksesMenu
                                        join Login in db.tbAdmLogin on Akses.UserName equals Login.UserName
                                        join Peran in db.tbAdmPeran on Login.KodePeran equals Peran.KodePeran
                                        join Menu in db.tbAdmMenu on Akses.KodeMenu equals Menu.KodeMenu
                                        where Menu.IsAktif == 1
                                        select 
                                        new
                                        {
                                            Akses.KodeMenu,
                                            Akses.UserName,
                                            Login.KodePeran,
                                            Peran.NamaPeran,
                                            Menu.NamaMenu,
                                            Menu.JenisMenu,
                                            Menu.KodeMenuParent,
                                            Menu.Controller,
                                            Menu.Action,
                                            Menu.Link,
                                            Menu.UrutanMenu,
                                            Menu.IsAktif,
                                            IsPunyaAnak = db.tbAdmMenu.Where(d=> d.KodeMenuParent == Menu.KodeMenu).Count()
                                        };
                foreach (var ListMenu in ListMenuOtorisasi)
                {
                    MenuOtorisasi ListAdd = new MenuOtorisasi()
                    {
                        KodeMenu = ListMenu.KodeMenu,
                        UserName = ListMenu.UserName,
                        KodePeran = ListMenu.KodePeran,
                        NamaPeran = ListMenu.NamaPeran,
                        NamaMenu = ListMenu.NamaMenu,
                        JenisMenu = ListMenu.JenisMenu,
                        KodeMenuParent = ListMenu.KodeMenuParent,
                        Controller = ListMenu.Controller,
                        Action = ListMenu.Action,
                        Link = ListMenu.Link,
                        UrutanMenu = ListMenu.UrutanMenu,
                        IsAktif = ListMenu.IsAktif,
                        IsPunyaAnak = ListMenu.IsPunyaAnak
                    };
                    List.Add(ListAdd);
                }
                return List;


            }
        }

        [HttpPost]
        public ActionResult UploadFile()
        {
            var fileUpload = Request.Files[0];
            
            var stream = fileUpload.InputStream;
            var FileSizeByte = fileUpload.ContentLength;
            var FileSize = FileSizeByte / 1000;
            var Extension = System.IO.Path.GetExtension(fileUpload.FileName);

            return View();
        }

        #region Jadwal Rekrutmen
        public List<JadwalRekrutmenModels> GetJadwalRekrutmenMingguIni()
        {
            try
            {
                List<JadwalRekrutmenModels> ListData = new List<JadwalRekrutmenModels>();

                DateTime dateFrom = GetDayFromThisWeek();
                DateTime dateTo = GetDayToThisWeek();

                var dtJadwalRekrutmen = db.vJadwalRekrutmen.Where(dt => dt.TanggalKedatangan >= dateFrom);
                dtJadwalRekrutmen = dtJadwalRekrutmen.Where(dt => dt.TanggalKedatangan <= dateTo);

                foreach (var itemDataJadwalRekrutmen in dtJadwalRekrutmen)
                {
                    JadwalRekrutmenModels JadwalRekrutmenModel = new JadwalRekrutmenModels()
                    {
                        KodeJadwalPemanggilan = itemDataJadwalRekrutmen.KodeJadwalPemanggilan,
                        KodeKandidat = itemDataJadwalRekrutmen.KodeKandidat,
                        JenisPemanggilan = itemDataJadwalRekrutmen.JenisPemanggilan,
                        TanggalPemanggilan = itemDataJadwalRekrutmen.TanggalPemanggilan,
                        TanggalKedatangan = itemDataJadwalRekrutmen.TanggalKedatangan,
                        KonfirmasiKedatangan = itemDataJadwalRekrutmen.KonfirmasiKedatangan,
                        Nama = itemDataJadwalRekrutmen.Nama,
                        TempatLahir = itemDataJadwalRekrutmen.TempatLahir,
                        TanggalLahir = itemDataJadwalRekrutmen.TanggalLahir,
                        JenisKelamin = itemDataJadwalRekrutmen.JenisKelamin,
                        Agama = itemDataJadwalRekrutmen.Agama,
                        Kewarganegaraan = itemDataJadwalRekrutmen.Kewarganegaraan,
                        NoTelpon = itemDataJadwalRekrutmen.NoTelpon,
                        KotaTinggal = itemDataJadwalRekrutmen.KotaTinggal,
                        JabatanYangDiajukan = itemDataJadwalRekrutmen.JabatanYangDiajukan,
                        PengalamanTeknologi = itemDataJadwalRekrutmen.PengalamanTeknologi,
                        PendidikanTerakhir = itemDataJadwalRekrutmen.PendidikanTerakhir,
                        LamaPengalamanTahunBekerja = itemDataJadwalRekrutmen.LamaPengalamanTahunBekerja,
                        TanggalInput = itemDataJadwalRekrutmen.TanggalInput,
                        KodeDokumen = itemDataJadwalRekrutmen.KodeDokumen
                    };
                    ListData.Add(JadwalRekrutmenModel);
                }

                return ListData;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<JadwalRekrutmenModels> GetJadwalRekrutmenBulanIni()
        {
            try
            {
                List<JadwalRekrutmenModels> ListData = new List<JadwalRekrutmenModels>();

                DateTime dateFrom = DateTime.Now.Date;
                DateTime LastDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month,
                                    DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
                
                var dtJadwalRekrutmen = db.vJadwalRekrutmen.Where(dt => dt.TanggalKedatangan >= dateFrom && dt.TanggalKedatangan <= LastDayOfMonth);

                foreach (var itemDataJadwalRekrutmen in dtJadwalRekrutmen)
                {
                    JadwalRekrutmenModels JadwalRekrutmenModel = new JadwalRekrutmenModels()
                    {
                        KodeJadwalPemanggilan = itemDataJadwalRekrutmen.KodeJadwalPemanggilan,
                        KodeKandidat = itemDataJadwalRekrutmen.KodeKandidat,
                        JenisPemanggilan = itemDataJadwalRekrutmen.JenisPemanggilan,
                        TanggalPemanggilan = itemDataJadwalRekrutmen.TanggalPemanggilan,
                        TanggalKedatangan = itemDataJadwalRekrutmen.TanggalKedatangan,
                        KonfirmasiKedatangan = itemDataJadwalRekrutmen.KonfirmasiKedatangan,
                        Nama = itemDataJadwalRekrutmen.Nama,
                        TempatLahir = itemDataJadwalRekrutmen.TempatLahir,
                        TanggalLahir = itemDataJadwalRekrutmen.TanggalLahir,
                        JenisKelamin = itemDataJadwalRekrutmen.JenisKelamin,
                        Agama = itemDataJadwalRekrutmen.Agama,
                        Kewarganegaraan = itemDataJadwalRekrutmen.Kewarganegaraan,
                        NoTelpon = itemDataJadwalRekrutmen.NoTelpon,
                        KotaTinggal = itemDataJadwalRekrutmen.KotaTinggal,
                        JabatanYangDiajukan = itemDataJadwalRekrutmen.JabatanYangDiajukan,
                        PengalamanTeknologi = itemDataJadwalRekrutmen.PengalamanTeknologi,
                        PendidikanTerakhir = itemDataJadwalRekrutmen.PendidikanTerakhir,
                        LamaPengalamanTahunBekerja = itemDataJadwalRekrutmen.LamaPengalamanTahunBekerja,
                        TanggalInput = itemDataJadwalRekrutmen.TanggalInput,
                        KodeDokumen = itemDataJadwalRekrutmen.KodeDokumen
                    };
                    ListData.Add(JadwalRekrutmenModel);
                }

                return ListData;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<JadwalRekrutmenModels> GetJadwalRekrutmenBulanDepan()
        {
            try
            {
                List<JadwalRekrutmenModels> ListData = new List<JadwalRekrutmenModels>();

                DateTime dateFrom = new DateTime(DateTime.Now.AddMonths(1).Year, DateTime.Now.AddMonths(1).Month, 1);
                DateTime LastDayOfNextMonth = new DateTime(DateTime.Now.AddMonths(1).Year, DateTime.Now.AddMonths(1).Month,
                                    DateTime.DaysInMonth(DateTime.Now.AddMonths(1).Year, DateTime.Now.AddMonths(1).Month));

                var dtJadwalRekrutmen = db.vJadwalRekrutmen.Where(dt => dt.TanggalKedatangan >= dateFrom && dt.TanggalKedatangan <= LastDayOfNextMonth);

                foreach (var itemDataJadwalRekrutmen in dtJadwalRekrutmen)
                {
                    JadwalRekrutmenModels JadwalRekrutmenModel = new JadwalRekrutmenModels()
                    {
                        KodeJadwalPemanggilan = itemDataJadwalRekrutmen.KodeJadwalPemanggilan,
                        KodeKandidat = itemDataJadwalRekrutmen.KodeKandidat,
                        JenisPemanggilan = itemDataJadwalRekrutmen.JenisPemanggilan,
                        TanggalPemanggilan = itemDataJadwalRekrutmen.TanggalPemanggilan,
                        TanggalKedatangan = itemDataJadwalRekrutmen.TanggalKedatangan,
                        KonfirmasiKedatangan = itemDataJadwalRekrutmen.KonfirmasiKedatangan,
                        Nama = itemDataJadwalRekrutmen.Nama,
                        TempatLahir = itemDataJadwalRekrutmen.TempatLahir,
                        TanggalLahir = itemDataJadwalRekrutmen.TanggalLahir,
                        JenisKelamin = itemDataJadwalRekrutmen.JenisKelamin,
                        Agama = itemDataJadwalRekrutmen.Agama,
                        Kewarganegaraan = itemDataJadwalRekrutmen.Kewarganegaraan,
                        NoTelpon = itemDataJadwalRekrutmen.NoTelpon,
                        KotaTinggal = itemDataJadwalRekrutmen.KotaTinggal,
                        JabatanYangDiajukan = itemDataJadwalRekrutmen.JabatanYangDiajukan,
                        PengalamanTeknologi = itemDataJadwalRekrutmen.PengalamanTeknologi,
                        PendidikanTerakhir = itemDataJadwalRekrutmen.PendidikanTerakhir,
                        LamaPengalamanTahunBekerja = itemDataJadwalRekrutmen.LamaPengalamanTahunBekerja,
                        TanggalInput = itemDataJadwalRekrutmen.TanggalInput,
                        KodeDokumen = itemDataJadwalRekrutmen.KodeDokumen
                    };
                    ListData.Add(JadwalRekrutmenModel);
                }

                return ListData;
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region Helper
        public DateTime GetDayFromThisWeek()
        {
            int loop = 5;
            DateTime currentDate = DateTime.Now;
            string currentDateName = DateTime.Now.ToString("dddd");
            DateTime dateFrom = DateTime.Now;

            for (int i = 0; i < loop; i++)
            {
                string dayName = DateTime.Now.AddDays(-i).ToString("dddd");

                if (dayName.ToLower() == "monday" || dayName.ToLower() == "senin")
                {
                    dateFrom = DateTime.Now.AddDays(-i);
                    i = 5;
                }
            }

            return dateFrom;
        }

        public DateTime GetDayToThisWeek()
        {
            int loop = 5;
            DateTime currentDate = DateTime.Now;
            string currentDateName = DateTime.Now.ToString("dddd");
            DateTime dateTo = DateTime.Now;

            for (int i = 0; i < loop; i++)
            {
                string dayName = DateTime.Now.AddDays(+i).ToString("dddd");

                if (dayName.ToLower() == "friday" || dayName.ToLower() == "jumat")
                {
                    dateTo = DateTime.Now.AddDays(+i);
                    i = 5;
                }
            }

            return dateTo;
        }
        #endregion
    }
}