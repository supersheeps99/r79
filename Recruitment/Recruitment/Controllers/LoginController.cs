﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Recruitment.DAL;

namespace Recruitment.Controllers
{
    public class LoginController : Controller
    {
        private RecruitmentDbContext db = new RecruitmentDbContext();

        // GET: Login
        public ActionResult Index()
        {
            var tbAdmLogin = db.tbAdmLogin.Include(t => t.tbAdmPeran);
            return View(tbAdmLogin.ToList());
        }

        // GET: Login/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbAdmLogin tbAdmLogin = db.tbAdmLogin.Find(id);
            if (tbAdmLogin == null)
            {
                return HttpNotFound();
            }
            return View(tbAdmLogin);
        }

        // GET: Login/Create
        public ActionResult Create()
        {
            ViewBag.KodePeran = new SelectList(db.tbAdmPeran, "KodePeran", "NamaPeran");
            return View();
        }

        // POST: Login/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KodeLogin,UserName,Password,KodePeran")] tbAdmLogin tbAdmLogin)
        {
            if (ModelState.IsValid)
            {
                tbAdmLogin.TanggalDibuat = DateTime.Now.Date;
                tbAdmLogin.Pengguna = "Admin";
                db.tbAdmLogin.Add(tbAdmLogin);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.KodePeran = new SelectList(db.tbAdmPeran, "KodePeran", "NamaPeran", tbAdmLogin.KodePeran);
            return View(tbAdmLogin);
        }

        // GET: Login/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbAdmLogin tbAdmLogin = db.tbAdmLogin.Find(id);
            if (tbAdmLogin == null)
            {
                return HttpNotFound();
            }
            ViewBag.KodePeran = new SelectList(db.tbAdmPeran, "KodePeran", "NamaPeran", tbAdmLogin.KodePeran);
            return View(tbAdmLogin);
        }

        // POST: Login/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "KodeLogin,UserName,Password,KodePeran")] tbAdmLogin tbAdmLogin)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbAdmLogin).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KodePeran = new SelectList(db.tbAdmPeran, "KodePeran", "NamaPeran", tbAdmLogin.KodePeran);
            return View(tbAdmLogin);
        }

        // GET: Login/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbAdmLogin tbAdmLogin = db.tbAdmLogin.Find(id);
            if (tbAdmLogin == null)
            {
                return HttpNotFound();
            }
            return View(tbAdmLogin);
        }

        // POST: Login/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            tbAdmLogin tbAdmLogin = db.tbAdmLogin.Find(id);
            db.tbAdmLogin.Remove(tbAdmLogin);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
