﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Recruitment.DAL;
using System.ComponentModel.DataAnnotations;

namespace Recruitment.Controllers
{
    public class NilaiPsikologiController : Controller
    {
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime TanggalProsesRekrutmen { get; set; }

        private RecruitmentDbContext db = new RecruitmentDbContext();

        // GET: NilaiPsikologi
        public ActionResult Index()
        {
            var tbNilaiPsikologi = db.tbNilaiPsikologi.Include(t => t.tbProsesRekrutmen);
            return View(tbNilaiPsikologi.ToList());
        }

        // GET: NilaiPsikologi/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbNilaiPsikologi tbNilaiPsikologi = db.tbNilaiPsikologi.Find(id);
            if (tbNilaiPsikologi == null)
            {
                return HttpNotFound();
            }
            return View(tbNilaiPsikologi);
        }

        // GET: NilaiPsikologi/Create
        public ActionResult Create()
        {
            try
            {
                if (!string.IsNullOrEmpty(Url.RequestContext.RouteData.Values["id"].ToString()))
                {
                    int id = Convert.ToInt32(Url.RequestContext.RouteData.Values["id"].ToString());
                    ViewBag.KodeProsesRekrutmen = new SelectList(db.vProsesRekrutmenKandidat.Where(dt => dt.KodeProsesRekrutmen == id), "KodeProsesRekrutmen", "Nama");
                    var vTanggalProsesRekrutmen = db.vProsesRekrutmenKandidat.Where(dt => dt.KodeProsesRekrutmen == id).Select(dt => dt.TanggalProsesRekrutmen).SingleOrDefault();
                    ViewBag.TanggalProsesRekrutmen = vTanggalProsesRekrutmen.ToString();
                    ViewBag.Display = "";
                }
                else
                {
                    ViewBag.KodeProsesRekrutmen = new SelectList(db.vProsesRekrutmenKandidat, "KodeProsesRekrutmen", "Nama");
                    ViewBag.Display = "style=display:none";
                }
            }
            catch (Exception)
            {
                ViewBag.KodeProsesRekrutmen = new SelectList(db.vProsesRekrutmenKandidat, "KodeProsesRekrutmen", "Nama");
                ViewBag.TanggalProsesRekrutmen = DateTime.Now.Date.ToShortDateString();
                ViewBag.Display = "style=display:none";
            }

            return View();
        }

        // POST: NilaiPsikologi/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KodeNilaiPsikolog,KodeProsesRekrutmen,DeksripsiHasilTest,KesimpulanHasilTest,KodeDokumen")] tbNilaiPsikologi tbNilaiPsikologi)
        {
            if (ModelState.IsValid)
            {
                tbNilaiPsikologi.TanggalDibuat = DateTime.Now.Date;
                tbNilaiPsikologi.Pengguna = "Admin";
                db.tbNilaiPsikologi.Add(tbNilaiPsikologi);
                db.SaveChanges();
                return RedirectToAction("Index", "ProsesRekrutmen");
            }

            ViewBag.KodeProsesRekrutmen = new SelectList(db.tbProsesRekrutmen, "KodeProsesRekrutmen", "Pengguna", tbNilaiPsikologi.KodeProsesRekrutmen);
            return View(tbNilaiPsikologi);
        }

        // GET: NilaiPsikologi/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbNilaiPsikologi tbNilaiPsikologi = db.tbNilaiPsikologi.Find(id);
            if (tbNilaiPsikologi == null)
            {
                return HttpNotFound();
            }
            //ViewBag.KodeProsesRekrutmen = new SelectList(db.tbProsesRekrutmen, "KodeProsesRekrutmen", "Nama", tbNilaiPsikologi.KodeProsesRekrutmen);
            ViewBag.KodeProsesRekrutmen = new SelectList(db.vProsesRekrutmenKandidat.Where(tb => tb.KodeProsesRekrutmen == tbNilaiPsikologi.KodeProsesRekrutmen), "KodeProsesRekrutmen", "Nama");
            return View(tbNilaiPsikologi);
        }

        // POST: NilaiPsikologi/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "KodeNilaiPsikolog,KodeProsesRekrutmen,DeksripsiHasilTest,KesimpulanHasilTest,KodeDokumen")] tbNilaiPsikologi tbNilaiPsikologi)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbNilaiPsikologi).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KodeProsesRekrutmen = new SelectList(db.tbProsesRekrutmen, "KodeProsesRekrutmen", "Pengguna", tbNilaiPsikologi.KodeProsesRekrutmen);
            return View(tbNilaiPsikologi);
        }

        // GET: NilaiPsikologi/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbNilaiPsikologi tbNilaiPsikologi = db.tbNilaiPsikologi.Find(id);
            if (tbNilaiPsikologi == null)
            {
                return HttpNotFound();
            }
            return View(tbNilaiPsikologi);
        }

        // POST: NilaiPsikologi/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tbNilaiPsikologi tbNilaiPsikologi = db.tbNilaiPsikologi.Find(id);
            db.tbNilaiPsikologi.Remove(tbNilaiPsikologi);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
