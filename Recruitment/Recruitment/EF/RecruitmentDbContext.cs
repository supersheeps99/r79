﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Recruitment.EF
{
    public class RecruitmentDbContext : DbContext
    {
        public RecruitmentDbContext() : base("RecruitmentDbContext")
        { }
    }
}