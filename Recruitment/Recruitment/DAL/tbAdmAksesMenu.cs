//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Recruitment.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbAdmAksesMenu
    {
        public int KodeAkses { get; set; }
        public string UserName { get; set; }
        public Nullable<int> KodeMenu { get; set; }
        public Nullable<System.DateTime> TanggalDibuat { get; set; }
        public string Pengguna { get; set; }
    
        public virtual tbAdmLogin tbAdmLogin { get; set; }
        public virtual tbAdmMenu tbAdmMenu { get; set; }
    }
}
