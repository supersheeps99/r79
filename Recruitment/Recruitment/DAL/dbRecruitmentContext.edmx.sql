
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 02/19/2019 09:59:02
-- Generated from EDMX file: E:\Natia\clone git\79Rekrutmen\Recruitment\Recruitment\DAL\dbRecruitmentContext.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [dbRecruitment];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_tbAdmAksesMenu_tbAdmLogin]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbAdmAksesMenu] DROP CONSTRAINT [FK_tbAdmAksesMenu_tbAdmLogin];
GO
IF OBJECT_ID(N'[dbo].[FK_tbAdmAksesMenu_tbAdmMenu]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbAdmAksesMenu] DROP CONSTRAINT [FK_tbAdmAksesMenu_tbAdmMenu];
GO
IF OBJECT_ID(N'[dbo].[FK_tbAdmLogin_tbAdmPeran]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbAdmLogin] DROP CONSTRAINT [FK_tbAdmLogin_tbAdmPeran];
GO
IF OBJECT_ID(N'[dbo].[FK_tblProsesRekrutmen_tbKandidat]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbProsesRekrutmen] DROP CONSTRAINT [FK_tblProsesRekrutmen_tbKandidat];
GO
IF OBJECT_ID(N'[dbo].[FK_tbNilaiPseudocode_tbProsesRekrutmen]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbNilaiPseudocode] DROP CONSTRAINT [FK_tbNilaiPseudocode_tbProsesRekrutmen];
GO
IF OBJECT_ID(N'[dbo].[FK_tbNilaiPsikologi_tbProsesRekrutmen]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbNilaiPsikologi] DROP CONSTRAINT [FK_tbNilaiPsikologi_tbProsesRekrutmen];
GO
IF OBJECT_ID(N'[dbo].[FK_tbPenilaianHasilTest_tbProsesRekrutmen]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbPenilaianHasilTest] DROP CONSTRAINT [FK_tbPenilaianHasilTest_tbProsesRekrutmen];
GO
IF OBJECT_ID(N'[dbo].[FK_tbProsesRekrutmen_tbNilaiAptitude]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbProsesRekrutmen] DROP CONSTRAINT [FK_tbProsesRekrutmen_tbNilaiAptitude];
GO
IF OBJECT_ID(N'[dbo].[FK_tbWawancara_tbProsesRekrutmen]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbWawancara] DROP CONSTRAINT [FK_tbWawancara_tbProsesRekrutmen];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[sysdiagrams]', 'U') IS NOT NULL
    DROP TABLE [dbo].[sysdiagrams];
GO
IF OBJECT_ID(N'[dbo].[tbAdmAksesMenu]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbAdmAksesMenu];
GO
IF OBJECT_ID(N'[dbo].[tbAdmLogin]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbAdmLogin];
GO
IF OBJECT_ID(N'[dbo].[tbAdmMenu]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbAdmMenu];
GO
IF OBJECT_ID(N'[dbo].[tbAdmPeran]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbAdmPeran];
GO
IF OBJECT_ID(N'[dbo].[tbJadwal]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbJadwal];
GO
IF OBJECT_ID(N'[dbo].[tbKandidat]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbKandidat];
GO
IF OBJECT_ID(N'[dbo].[tbManagemenDokumen]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbManagemenDokumen];
GO
IF OBJECT_ID(N'[dbo].[tbNilaiAptitude]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbNilaiAptitude];
GO
IF OBJECT_ID(N'[dbo].[tbNilaiPseudocode]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbNilaiPseudocode];
GO
IF OBJECT_ID(N'[dbo].[tbNilaiPsikologi]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbNilaiPsikologi];
GO
IF OBJECT_ID(N'[dbo].[tbPenilaianHasilTest]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbPenilaianHasilTest];
GO
IF OBJECT_ID(N'[dbo].[tbProsesRekrutmen]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbProsesRekrutmen];
GO
IF OBJECT_ID(N'[dbo].[tbWawancara]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbWawancara];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'sysdiagrams'
CREATE TABLE [dbo].[sysdiagrams] (
    [name] nvarchar(128)  NOT NULL,
    [principal_id] int  NOT NULL,
    [diagram_id] int IDENTITY(1,1) NOT NULL,
    [version] int  NULL,
    [definition] varbinary(max)  NULL
);
GO

-- Creating table 'tbAdmAksesMenu'
CREATE TABLE [dbo].[tbAdmAksesMenu] (
    [KodeAkses] int IDENTITY(1,1) NOT NULL,
    [UserName] varchar(64)  NOT NULL,
    [KodeMenu] int  NULL,
    [TanggalDibuat] datetime  NULL,
    [Pengguna] varchar(64)  NULL
);
GO

-- Creating table 'tbAdmLogin'
CREATE TABLE [dbo].[tbAdmLogin] (
    [KodeLogin] int IDENTITY(1,1) NOT NULL,
    [UserName] varchar(64)  NOT NULL,
    [Password] varchar(64)  NULL,
    [KodePeran] int  NOT NULL,
    [TanggalDibuat] datetime  NULL,
    [Pengguna] varchar(64)  NULL
);
GO

-- Creating table 'tbAdmMenu'
CREATE TABLE [dbo].[tbAdmMenu] (
    [KodeMenu] int IDENTITY(1,1) NOT NULL,
    [NamaMenu] varchar(64)  NULL,
    [JenisMenu] varchar(16)  NULL,
    [KodeMenuParent] int  NULL,
    [Controller] varchar(64)  NULL,
    [Action] varchar(64)  NULL,
    [Link] varchar(128)  NULL,
    [UrutanMenu] int  NULL,
    [IsAktif] int  NULL,
    [TanggalDibuat] datetime  NULL,
    [Pengguna] varchar(64)  NULL
);
GO

-- Creating table 'tbAdmPeran'
CREATE TABLE [dbo].[tbAdmPeran] (
    [KodePeran] int IDENTITY(1,1) NOT NULL,
    [NamaPeran] varchar(64)  NULL
);
GO

-- Creating table 'tbJadwal'
CREATE TABLE [dbo].[tbJadwal] (
    [KodeJadwalPemanggilan] int IDENTITY(1,1) NOT NULL,
    [KodeKandidat] int  NULL,
    [JenisPemanggilan] varchar(64)  NULL,
    [TanggalPemanggilan] datetime  NULL,
    [TanggalKedatangan] datetime  NULL,
    [KonfirmasiKedatangan] char(1)  NULL,
    [TanggalDibuat] datetime  NULL,
    [Pengguna] varchar(64)  NULL
);
GO

-- Creating table 'tbKandidat'
CREATE TABLE [dbo].[tbKandidat] (
    [KodeKandidat] int IDENTITY(1,1) NOT NULL,
    [Nama] varchar(128)  NOT NULL,
    [TempatLahir] varchar(64)  NULL,
    [TanggalLahir] datetime  NULL,
    [JenisKelamin] varchar(16)  NULL,
    [Agama] varchar(8)  NULL,
    [Kewarganegaraan] varchar(16)  NULL,
    [NoTelpon] varchar(16)  NOT NULL,
    [KotaTinggal] varchar(64)  NOT NULL,
    [JabatanYangDiajukan] varchar(64)  NULL,
    [PengalamanTeknologi] varchar(256)  NULL,
    [PendidikanTerakhir] varchar(128)  NULL,
    [LamaPengalamanTahunBekerja] int  NULL,
    [TanggalInput] datetime  NOT NULL,
    [KodeDokumen] int  NULL,
    [TanggalDibuat] datetime  NULL,
    [Pengguna] varchar(64)  NULL
);
GO

-- Creating table 'tbNilaiAptitude'
CREATE TABLE [dbo].[tbNilaiAptitude] (
    [KodeNilaiAptitude] int IDENTITY(1,1) NOT NULL,
    [KodeProsesRekrutmen] int  NOT NULL,
    [NilaiAll] int  NULL,
    [NilaiKompleks] int  NULL,
    [NilaiLogika] int  NULL,
    [NilaiPola] int  NULL,
    [Grade] varchar(32)  NULL,
    [TanggalDibuat] datetime  NULL,
    [Pengguna] varchar(64)  NULL
);
GO

-- Creating table 'tbNilaiPseudocode'
CREATE TABLE [dbo].[tbNilaiPseudocode] (
    [KodeNilaiPsudecode] int IDENTITY(1,1) NOT NULL,
    [KodeProsesRekrutmen] int  NOT NULL,
    [NoSoal] int  NULL,
    [DeskripsiHasilTest] varchar(max)  NULL,
    [Kesimpulan] varchar(max)  NULL,
    [GradeNilai] varchar(16)  NULL,
    [TanggalDibuat] datetime  NULL,
    [Pengguna] varchar(64)  NULL
);
GO

-- Creating table 'tbNilaiPsikologi'
CREATE TABLE [dbo].[tbNilaiPsikologi] (
    [KodeNilaiPsikolog] int IDENTITY(1,1) NOT NULL,
    [KodeProsesRekrutmen] int  NOT NULL,
    [DeksripsiHasilTest] varchar(max)  NULL,
    [KesimpulanHasilTest] varchar(max)  NULL,
    [KodeDokumen] varchar(128)  NULL,
    [TanggalDibuat] datetime  NULL,
    [Pengguna] varchar(64)  NULL
);
GO

-- Creating table 'tbPenilaianHasilTest'
CREATE TABLE [dbo].[tbPenilaianHasilTest] (
    [KodePenilaianHasilTes] int  NOT NULL,
    [KodeProsesRekrutmen] int  NOT NULL,
    [TanggalPenilaian] datetime  NOT NULL,
    [StatusTest] varchar(32)  NULL,
    [Keterangan] varchar(max)  NULL,
    [TanggalDibuat] datetime  NULL,
    [Pengguna] varchar(64)  NULL
);
GO

-- Creating table 'tbWawancara'
CREATE TABLE [dbo].[tbWawancara] (
    [KodeWawancara] int  NOT NULL,
    [KodeProsesRekrutmen] int  NOT NULL,
    [TanggalWawancara] datetime  NOT NULL,
    [DeskripsiHasilWawancara] varchar(max)  NULL,
    [KesimpulanWawancara] varchar(max)  NULL,
    [TanggalDibuat] datetime  NULL,
    [Pengguna] varchar(64)  NULL
);
GO

-- Creating table 'tbManagemenDokumen'
CREATE TABLE [dbo].[tbManagemenDokumen] (
    [KodeDokumen] int  NOT NULL,
    [NamaDokumen] varchar(256)  NULL,
    [TanggalDibuat] datetime  NULL,
    [Pengguna] varchar(64)  NULL,
    [EkstensiFine] varchar(8)  NULL,
    [AlamatFile] varchar(256)  NULL
);
GO

-- Creating table 'tbProsesRekrutmen'
CREATE TABLE [dbo].[tbProsesRekrutmen] (
    [KodeProsesRekrutmen] int IDENTITY(1,1) NOT NULL,
    [TanggalProsesRekrutmen] datetime  NOT NULL,
    [KodeKandidat] int  NOT NULL,
    [ProsesStep] int  NOT NULL,
    [TanggalDibuat] datetime  NULL,
    [Pengguna] varchar(64)  NULL
);
GO

-- Creating table 'vProsesRekrutmenKandidat'
CREATE TABLE [dbo].[vProsesRekrutmenKandidat] (
    [KodeProsesRekrutmen] int  NOT NULL,
    [TanggalProsesRekrutmen] datetime  NOT NULL,
    [KodeKandidat] int  NOT NULL,
    [Nama] varchar(128)  NOT NULL,
    [ProsesStep] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [diagram_id] in table 'sysdiagrams'
ALTER TABLE [dbo].[sysdiagrams]
ADD CONSTRAINT [PK_sysdiagrams]
    PRIMARY KEY CLUSTERED ([diagram_id] ASC);
GO

-- Creating primary key on [KodeAkses] in table 'tbAdmAksesMenu'
ALTER TABLE [dbo].[tbAdmAksesMenu]
ADD CONSTRAINT [PK_tbAdmAksesMenu]
    PRIMARY KEY CLUSTERED ([KodeAkses] ASC);
GO

-- Creating primary key on [UserName] in table 'tbAdmLogin'
ALTER TABLE [dbo].[tbAdmLogin]
ADD CONSTRAINT [PK_tbAdmLogin]
    PRIMARY KEY CLUSTERED ([UserName] ASC);
GO

-- Creating primary key on [KodeMenu] in table 'tbAdmMenu'
ALTER TABLE [dbo].[tbAdmMenu]
ADD CONSTRAINT [PK_tbAdmMenu]
    PRIMARY KEY CLUSTERED ([KodeMenu] ASC);
GO

-- Creating primary key on [KodePeran] in table 'tbAdmPeran'
ALTER TABLE [dbo].[tbAdmPeran]
ADD CONSTRAINT [PK_tbAdmPeran]
    PRIMARY KEY CLUSTERED ([KodePeran] ASC);
GO

-- Creating primary key on [KodeJadwalPemanggilan] in table 'tbJadwal'
ALTER TABLE [dbo].[tbJadwal]
ADD CONSTRAINT [PK_tbJadwal]
    PRIMARY KEY CLUSTERED ([KodeJadwalPemanggilan] ASC);
GO

-- Creating primary key on [KodeKandidat] in table 'tbKandidat'
ALTER TABLE [dbo].[tbKandidat]
ADD CONSTRAINT [PK_tbKandidat]
    PRIMARY KEY CLUSTERED ([KodeKandidat] ASC);
GO

-- Creating primary key on [KodeNilaiAptitude] in table 'tbNilaiAptitude'
ALTER TABLE [dbo].[tbNilaiAptitude]
ADD CONSTRAINT [PK_tbNilaiAptitude]
    PRIMARY KEY CLUSTERED ([KodeNilaiAptitude] ASC);
GO

-- Creating primary key on [KodeNilaiPsudecode] in table 'tbNilaiPseudocode'
ALTER TABLE [dbo].[tbNilaiPseudocode]
ADD CONSTRAINT [PK_tbNilaiPseudocode]
    PRIMARY KEY CLUSTERED ([KodeNilaiPsudecode] ASC);
GO

-- Creating primary key on [KodeNilaiPsikolog] in table 'tbNilaiPsikologi'
ALTER TABLE [dbo].[tbNilaiPsikologi]
ADD CONSTRAINT [PK_tbNilaiPsikologi]
    PRIMARY KEY CLUSTERED ([KodeNilaiPsikolog] ASC);
GO

-- Creating primary key on [KodePenilaianHasilTes] in table 'tbPenilaianHasilTest'
ALTER TABLE [dbo].[tbPenilaianHasilTest]
ADD CONSTRAINT [PK_tbPenilaianHasilTest]
    PRIMARY KEY CLUSTERED ([KodePenilaianHasilTes] ASC);
GO

-- Creating primary key on [KodeWawancara] in table 'tbWawancara'
ALTER TABLE [dbo].[tbWawancara]
ADD CONSTRAINT [PK_tbWawancara]
    PRIMARY KEY CLUSTERED ([KodeWawancara] ASC);
GO

-- Creating primary key on [KodeDokumen] in table 'tbManagemenDokumen'
ALTER TABLE [dbo].[tbManagemenDokumen]
ADD CONSTRAINT [PK_tbManagemenDokumen]
    PRIMARY KEY CLUSTERED ([KodeDokumen] ASC);
GO

-- Creating primary key on [KodeProsesRekrutmen] in table 'tbProsesRekrutmen'
ALTER TABLE [dbo].[tbProsesRekrutmen]
ADD CONSTRAINT [PK_tbProsesRekrutmen]
    PRIMARY KEY CLUSTERED ([KodeProsesRekrutmen] ASC);
GO

-- Creating primary key on [KodeProsesRekrutmen], [TanggalProsesRekrutmen], [KodeKandidat], [Nama], [ProsesStep] in table 'vProsesRekrutmenKandidat'
ALTER TABLE [dbo].[vProsesRekrutmenKandidat]
ADD CONSTRAINT [PK_vProsesRekrutmenKandidat]
    PRIMARY KEY CLUSTERED ([KodeProsesRekrutmen], [TanggalProsesRekrutmen], [KodeKandidat], [Nama], [ProsesStep] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [UserName] in table 'tbAdmAksesMenu'
ALTER TABLE [dbo].[tbAdmAksesMenu]
ADD CONSTRAINT [FK_tbAdmAksesMenu_tbAdmLogin]
    FOREIGN KEY ([UserName])
    REFERENCES [dbo].[tbAdmLogin]
        ([UserName])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_tbAdmAksesMenu_tbAdmLogin'
CREATE INDEX [IX_FK_tbAdmAksesMenu_tbAdmLogin]
ON [dbo].[tbAdmAksesMenu]
    ([UserName]);
GO

-- Creating foreign key on [KodeMenu] in table 'tbAdmAksesMenu'
ALTER TABLE [dbo].[tbAdmAksesMenu]
ADD CONSTRAINT [FK_tbAdmAksesMenu_tbAdmMenu]
    FOREIGN KEY ([KodeMenu])
    REFERENCES [dbo].[tbAdmMenu]
        ([KodeMenu])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_tbAdmAksesMenu_tbAdmMenu'
CREATE INDEX [IX_FK_tbAdmAksesMenu_tbAdmMenu]
ON [dbo].[tbAdmAksesMenu]
    ([KodeMenu]);
GO

-- Creating foreign key on [KodePeran] in table 'tbAdmLogin'
ALTER TABLE [dbo].[tbAdmLogin]
ADD CONSTRAINT [FK_tbAdmLogin_tbAdmPeran]
    FOREIGN KEY ([KodePeran])
    REFERENCES [dbo].[tbAdmPeran]
        ([KodePeran])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_tbAdmLogin_tbAdmPeran'
CREATE INDEX [IX_FK_tbAdmLogin_tbAdmPeran]
ON [dbo].[tbAdmLogin]
    ([KodePeran]);
GO

-- Creating foreign key on [KodeKandidat] in table 'tbProsesRekrutmen'
ALTER TABLE [dbo].[tbProsesRekrutmen]
ADD CONSTRAINT [FK_tblProsesRekrutmen_tbKandidat]
    FOREIGN KEY ([KodeKandidat])
    REFERENCES [dbo].[tbKandidat]
        ([KodeKandidat])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_tblProsesRekrutmen_tbKandidat'
CREATE INDEX [IX_FK_tblProsesRekrutmen_tbKandidat]
ON [dbo].[tbProsesRekrutmen]
    ([KodeKandidat]);
GO

-- Creating foreign key on [KodeProsesRekrutmen] in table 'tbNilaiPseudocode'
ALTER TABLE [dbo].[tbNilaiPseudocode]
ADD CONSTRAINT [FK_tbNilaiPseudocode_tbProsesRekrutmen]
    FOREIGN KEY ([KodeProsesRekrutmen])
    REFERENCES [dbo].[tbProsesRekrutmen]
        ([KodeProsesRekrutmen])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_tbNilaiPseudocode_tbProsesRekrutmen'
CREATE INDEX [IX_FK_tbNilaiPseudocode_tbProsesRekrutmen]
ON [dbo].[tbNilaiPseudocode]
    ([KodeProsesRekrutmen]);
GO

-- Creating foreign key on [KodeProsesRekrutmen] in table 'tbNilaiPsikologi'
ALTER TABLE [dbo].[tbNilaiPsikologi]
ADD CONSTRAINT [FK_tbNilaiPsikologi_tbProsesRekrutmen]
    FOREIGN KEY ([KodeProsesRekrutmen])
    REFERENCES [dbo].[tbProsesRekrutmen]
        ([KodeProsesRekrutmen])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_tbNilaiPsikologi_tbProsesRekrutmen'
CREATE INDEX [IX_FK_tbNilaiPsikologi_tbProsesRekrutmen]
ON [dbo].[tbNilaiPsikologi]
    ([KodeProsesRekrutmen]);
GO

-- Creating foreign key on [KodeProsesRekrutmen] in table 'tbPenilaianHasilTest'
ALTER TABLE [dbo].[tbPenilaianHasilTest]
ADD CONSTRAINT [FK_tbPenilaianHasilTest_tbProsesRekrutmen]
    FOREIGN KEY ([KodeProsesRekrutmen])
    REFERENCES [dbo].[tbProsesRekrutmen]
        ([KodeProsesRekrutmen])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_tbPenilaianHasilTest_tbProsesRekrutmen'
CREATE INDEX [IX_FK_tbPenilaianHasilTest_tbProsesRekrutmen]
ON [dbo].[tbPenilaianHasilTest]
    ([KodeProsesRekrutmen]);
GO

-- Creating foreign key on [KodeProsesRekrutmen] in table 'tbProsesRekrutmen'
ALTER TABLE [dbo].[tbProsesRekrutmen]
ADD CONSTRAINT [FK_tbProsesRekrutmen_tbNilaiAptitude]
    FOREIGN KEY ([KodeProsesRekrutmen])
    REFERENCES [dbo].[tbProsesRekrutmen]
        ([KodeProsesRekrutmen])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [KodeProsesRekrutmen] in table 'tbWawancara'
ALTER TABLE [dbo].[tbWawancara]
ADD CONSTRAINT [FK_tbWawancara_tbProsesRekrutmen]
    FOREIGN KEY ([KodeProsesRekrutmen])
    REFERENCES [dbo].[tbProsesRekrutmen]
        ([KodeProsesRekrutmen])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_tbWawancara_tbProsesRekrutmen'
CREATE INDEX [IX_FK_tbWawancara_tbProsesRekrutmen]
ON [dbo].[tbWawancara]
    ([KodeProsesRekrutmen]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------