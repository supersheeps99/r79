//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Recruitment.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbAdmPeran
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbAdmPeran()
        {
            this.tbAdmLogin = new HashSet<tbAdmLogin>();
        }
    
        public int KodePeran { get; set; }
        public string NamaPeran { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbAdmLogin> tbAdmLogin { get; set; }
    }
}
