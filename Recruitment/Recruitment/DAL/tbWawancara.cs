//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Recruitment.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbWawancara
    {
        public int KodeWawancara { get; set; }
        public int KodeProsesRekrutmen { get; set; }
        public System.DateTime TanggalWawancara { get; set; }
        public string DeskripsiHasilWawancara { get; set; }
        public string KesimpulanWawancara { get; set; }
        public Nullable<System.DateTime> TanggalDibuat { get; set; }
        public string Pengguna { get; set; }
    
        public virtual tbProsesRekrutmen tbProsesRekrutmen { get; set; }
    }
}
