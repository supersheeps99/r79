﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Recruitment
{
    public class MenuOtorisasi
    {
        public int? KodeMenu { get; set; }
        public string UserName { get; set; }
        public int? KodePeran { get; set; }
        public string NamaPeran { get; set; }
        public string NamaMenu { get; set; }
        public string JenisMenu { get; set; }
        public int? KodeMenuParent { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Link { get; set; }
        public int? UrutanMenu { get; set; }
        public int? IsAktif { get; set; }
        public int? IsPunyaAnak { get; set; }
    }

    public class ViewModel
    {
        public IEnumerable<MenuOtorisasi> MenuOtorisasi { get; set; }
        //public IQueryable<MenuOtorisasi> MenuOtorisasi { get; set; }
    }
}