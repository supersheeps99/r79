﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Recruitment.Models
{
    public class JadwalRekrutmenModels
    {
        public int? KodeJadwalPemanggilan { get; set; }
        public int? KodeKandidat { get; set; }
        public string JenisPemanggilan { get; set; }
        public DateTime? TanggalPemanggilan { get; set; }
        public DateTime? TanggalKedatangan { get; set; }
        public string KonfirmasiKedatangan { get; set; }
        public string Nama { get; set; }
        public string TempatLahir { get; set; }
        public DateTime? TanggalLahir { get; set; }
        public string JenisKelamin { get; set; }
        public string Agama { get; set; }
        public string Kewarganegaraan { get; set; }
        public string NoTelpon { get; set; }
        public string KotaTinggal { get; set; }
        public string JabatanYangDiajukan { get; set; }
        public string PengalamanTeknologi { get; set; }
        public string PendidikanTerakhir { get; set; }
        public int? LamaPengalamanTahunBekerja { get; set; }
        public DateTime? TanggalInput { get; set; }
        public int? KodeDokumen { get; set; }
    }
}