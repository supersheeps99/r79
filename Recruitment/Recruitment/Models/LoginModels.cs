﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Recruitment.Models
{
    public class LoginModels
    {
        public int? ID { get; set; }
        public int? KodeLogin { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int? KodePeran { get; set; }
        public DateTime? TanggalDibuat { get; set; }
        public string Pengguna { get; set; }
    }
}